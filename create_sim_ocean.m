function [ocn_script]= create_sim_ocean(sim_netlist,out_dir)

[tw_in, v_in] = gen_sweep_pars(3,300*10^-12,27,27);

ocnHeader = './templts/ocnHeader.ocn';
out_waveform_file = [out_dir,'/sim.out'];
ocn_script = [out_dir,'/sim.ocn'];

t_head = fileread(ocnHeader);

t_design = [newline ,'design("',sim_netlist,'")'];

t_param = [ newline,'paramAnalysis("v_in" ?values ''(',num2str(v_in),')'...
                 ,newline,'paramAnalysis("tw_in" ?values ''(',num2str(tw_in),')'...
                 ,newline,'))'];

t_footer= [newline ,'paramRun()',...
            newline,'selectResults(''tran)',...
            newline,'out=outfile("',out_waveform_file,'" "w" )',...
            newline,'ocnPrint(?output out ?numberNotation ''engineering VT("/out_dut"))'];%,...
            %newline,'ocnPrint(?output out ?numberNotation ''engineering VT("/in_dut"))'];
f_out = fopen(ocn_script,'w');
fprintf(f_out,'%s',[t_head t_design t_param t_footer]);
end
