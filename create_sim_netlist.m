function [sim_netlist,instnce_name]=create_sim_netlist(gate_netlist_path,ports_conns)

% ports_conns = { 'A' 'B' 'C' 'Q'; 0 1 1 2};
%0 gnd
%1 Vdd
%2 test_input
%3 test_output
vdd_name='vdd3!';
TB_Netlist_path='./templts/TB.scs';
sim_netlist =gate_netlist_path;
%% open and read the two files to strings

if ~isfile([gate_netlist_path,'_gen'])
    movefile(gate_netlist_path,[gate_netlist_path,'_gen']);
end
 t_gate = fileread([gate_netlist_path,'_gen']);
t_tb = fileread(TB_Netlist_path);
%% get the instance (DUT Gate) line and modify it
indx =strfind(t_gate,'myinst0');
instantiation_line = t_gate(indx:end);%get the myinst0 line as it's the last line in the gate netlist
tokens = strsplit(instantiation_line,{' ','(',')'});
instnce_name = tokens{end};
if(size(ports_conns,2) ~= length(tokens)-4)
    error('size of conns provided doesnt match the gate ports');
end
ports={};
for i = 1:length(tokens)-4
    if(ports_conns{2,i}==0)
        ports{i}='0';
    elseif(ports_conns{2,i}==1)
         ports{i}=vdd_name;
    elseif(ports_conns{2,i}==2)
         ports{i}='in_dut';
    elseif(ports_conns{2,i}==3)
         ports{i}='out_dut';
    end
end
new_instnce_line=[newline ,tokens{1},' ( ',strjoin(ports),' ',strjoin(tokens(end-2:end-1)),' ) ',tokens{end}];
t_gate(indx:indx+1)=['//'];%comment the old myinst0 line
t_all= [t_gate t_tb new_instnce_line];
f_out = fopen(sim_netlist,'w');
fprintf(f_out,'%s',t_all);
fclose(f_out);


