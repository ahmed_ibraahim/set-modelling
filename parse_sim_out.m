%function [tw_in,v_in,tw_out,v_out]=parse_sim_out(sim_out_file)

clear
Vdd=3;
correct_output=Vdd;
dut_name = 'NA2_3VX0';
plot_trans=1;
filename=['~/simulation/',dut_name,'/my_run/sim.out'];
%filename='./sim/sim.out';
%filename='test3.csv';

v_threshold=0.0;

fid = fopen(filename);
sim_count=0;
while(~feof(fid))
sim_count=sim_count+1;
data_1= textscan(fid,'v_in = %f\ntw_in = %f','CommentStyle','\n','CommentStyle','time'); 
v_in(sim_count)= data_1{1};
tw_in(sim_count)=data_1{2};
data_2 = textscan(fid,'%f %f\n','CommentStyle','\n'); 

data_X{sim_count}= data_2{1};
data_Y{sim_count}= data_2{2};
 
end
fclose(fid);


for indx =1:sim_count%[390 391 392]
   
    data_X_filtered=data_X{indx};
    data_Y_filtered=data_Y{indx};
    if(correct_output > 0)
        data_Y_temp=correct_output-(data_Y_filtered);
    else
        data_Y_temp=data_Y_filtered;
    end
    if(plot_trans)
     figure
     %hold on
        plot(data_X_filtered,data_Y_filtered,'.');
    ttl=['tw\_in= ',tw_in(indx),'ps v\_in= ' ,v_in(indx)];
    title(ttl)
    end
    [v_out(indx), indx_max]=max(data_Y_temp);
    %tw_in(indx)=str2double(tw_in_val{indx});
    %v_in(indx)=str2double(v_in_val{indx});
    if(abs(v_out(indx))>v_threshold)
        difference = abs(v_out(indx)/2-data_Y_temp);
        [~,first_edge] = min(difference(1:indx_max-1));

        [~,secnd_edge] = min(difference(indx_max+1:end));
        if(indx_max < length(difference))
            secnd_edge=secnd_edge+indx_max;
        else
            secnd_edge = indx_max;
        end
        if(isempty(first_edge))
                first_edge=secnd_edge;
        end
        tw_out(indx)=abs(data_X_filtered(first_edge)-data_X_filtered(secnd_edge));
     if(plot_trans)
    hold on
    plot(data_X_filtered(first_edge),data_Y_filtered(first_edge),'ro');
    plot(data_X_filtered(secnd_edge),data_Y_filtered(secnd_edge),'ro');
     end
    else
        tw_out(indx)= 0;
    end

end
%%
figure(1)
tw_in=tw_in*10^12;
tw_out=tw_out*10^12;
%to plot surface
nn=length(unique(v_in));
v_in =reshape(v_in,nn,length(v_in)/nn);
tw_in =reshape(tw_in,nn,length(tw_in)/nn);
v_out =reshape(v_out,nn,length(v_out)/nn);
tw_out =reshape(tw_out,nn,length(tw_out)/nn);
%scatter3(tw_in,v_in,v_out);
surf(tw_in,v_in,v_out);
xlabel('tw\_in');ylabel('v\_in');zlabel('v\_out');
figure(2)
%scatter3(tw_in,v_in,tw_out);
surf(tw_in,v_in,tw_out);
xlabel('tw\_in');ylabel('v\_in');zlabel('tw\_out');
ylim([0 Vdd])
%
% v_in=v_in(3:end,:);
% tw_in=tw_in(3:end,:);
% v_out=v_out(3:end,:);
