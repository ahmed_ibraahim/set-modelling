%td1=64;
%VDC=1.483;
%gf=fittype('prop_model(alpha,c,T,VDC,td1,Vdd,tw_in,v_in)','coefficients',{'c','T','alpha'},'independent',{'tw_in','v_in'},'problem',{'Vdd','VDC','td1'});
gf=fittype('prop_model(alpha,c,T,VDC,td1,Vdd,tw_in,v_in)','coefficients',{'c','T','alpha','VDC','td1'},'independent',{'tw_in','v_in'},'problem','Vdd');
tw_in_l = reshape(tw_in,1,size(tw_in,1)*size(tw_in,2));
v_in_l = reshape(v_in,1,size(v_in,1)*size(v_in,2));
v_out_l=reshape(v_out,1,size(v_out,1)*size(v_out,2));

%v_out_l(v_out_l<0.001)=0;
% tw_in_l=tw_in_l(51:end);
% v_in_l=v_in_l(51:end);
% v_out_l=v_out_l(51:end);

options = fitoptions('Method','NonlinearLeastSquares','Algorithm','Levenberg-Marquardt','Robust','Off');
%options = fitoptions('gauss2');
options.StartPoint = [20 150 1 1.5 50];
options.StartPoint = [39.86 959.2 1.16 1.456 38.21];
%options.StartPoint = [0 0 0 0 0];
%options.Lower = [0 0 0 1 20];
%options.Upper = [0 100 0 0 0]+300;


%v_model=fit([tw_in_l',v_in_l'],v_out_l',gf,options,'problem',{Vdd,VDC,td1})
v_model=fit([tw_in_l',v_in_l'],v_out_l',gf,options,'problem',Vdd);
%v_model=fit([tw_in_l',v_in_l'],v_out_l','biharmonicinterp');
%%
figure(3)
subplot(2,2,1)
plot(v_model);
title('fitted model')
xlabel('tw\_in');ylabel('v\_in');zlabel('v\_out');
subplot(2,2,2)
plot(v_model,[tw_in_l',v_in_l'],v_out_l','Style','Residuals');
title('Residuals')
xlabel('tw\_in');ylabel('v\_in');zlabel('v\_out');

%%
% calc error
%for i=1:length(v_in_l)
%v_fit(i) = prop_model(v_model.alpha,v_model.c,v_model.T,v_model.VDC,v_model.td1,v_model.Vdd,tw_in_l(i),v_in_l(i));
%end
[max_err_v,indx] = max(abs(v_model(tw_in_l,v_in_l)-v_out_l))
%% fit width
gf=fittype('tw_out_model(a0,a1,b0,b1,t0,ti,tw_in,v_in)','coefficients',{'a0','a1','b0','b1','t0','ti'},'independent',{'tw_in','v_in'});
tw_out_l=reshape(tw_out,1,size(tw_out,1)*size(tw_out,2));
figure(4)
options = fitoptions('Method','NonlinearLeastSquares','Algorithm','Levenberg-Marquardt','Robust','Off');
%options = fitoptions('Method','NonlinearLeastSquares','Robust','Off');
%options.StartPoint = [1 0.4 0 0 20 100];
tw_model=fit([tw_in_l',v_in_l'],tw_out_l',gf,options);
%tw_model=fit([tw_in_l',v_in_l'],tw_out_l','biharmonicinterp');
subplot(2,2,3)
title('Plot of tw_out fitted model and Residuals')
plot(tw_model);
%ylim([Vdd/2 Vdd]);zlim([0 Inf]);
xlabel('tw\_in');ylabel('v\_in');zlabel('tw\_out');
subplot(2,2,4)
plot(tw_model,[tw_in_l',v_in_l'],tw_out_l','Style','Residuals');

%ylim([Vdd/2 Vdd]);
xlabel('tw\_in');ylabel('v\_in');zlabel('tw\_out');
%% save the model
save(['./models/',dut_name,'_model.mat'],'v_model','tw_model','max_err_v');
