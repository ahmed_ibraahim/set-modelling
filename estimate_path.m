dut_cells = {'IN_3VX4','NO2_3VX0','NA2_3VX0'}

tw_in=100;
v_in=2.9;

tw_temp = tw_in;
v_temp=v_in;
for i = 1:length(dut_cells)
    dut_name = dut_cells{i};
   load(['./models/',dut_name,'_model.mat']);
   v_out = v_model(tw_temp,v_temp);
   tw_out = tw_model(tw_temp,v_temp);
   if(i==1)
       tw_out=tw_out-50;
   end
   if (tw_out < 10)
       tw_out = 10;
   end
   fprintf('After dut %s Width: %g ps , Height: %g V\n',dut_name,tw_out,v_out);
   tw_temp = tw_out;
   v_temp=v_out;
end