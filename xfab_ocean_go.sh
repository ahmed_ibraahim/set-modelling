#!/bin/bash
cd "/home/ahmed/EDA/testXFAB_xh018"
echo "<<<Setting Calibre and Cadence Environments>>>"
source /home/ahmed/MentorHome/Calibre2018/calibre_setup.sh

source /home/ahmed/cadence/cadence_run_setup.sh
export X_DIR=/pdks/Xfab/Xh018
export PATH=$X_DIR/x_all/cadence/xenv:$PATH

export CDS_Netlisting_Mode="Analog"
export CDS_HOME=$CDSHOME
#echo "<<<Starting virtuoso with XFAB_Xh018 [using XKIT scripts] and Calibre Environments>>>"

xkit -t xh018 --modules 1131 -n

#To update the libs
#xkit -u -t xh018 --modules 1131




#ocean

