function [minV,maxV,avV,minT,maxT,avT] = calc_error(tw_in,v_in,tw_out,v_out)
Vdd=3;
gf=fittype('prop_model(alpha,c,T,VDC,td1,Vdd,tw_in,v_in)','coefficients',{'c','T','alpha','VDC','td1'},'independent',{'tw_in','v_in'},'problem','Vdd')
tw_in_l = reshape(tw_in,1,size(tw_in,1)*size(tw_in,2));
v_in_l = reshape(v_in,1,size(v_in,1)*size(v_in,2));
v_out_l=reshape(v_out,1,size(v_out,1)*size(v_out,2));
options = fitoptions('Method','NonlinearLeastSquares','Algorithm','Levenberg-Marquardt','Robust','Off');
options.StartPoint = [20 150 1 1.5 50];
[v_model,gof,rv_model]=fit([tw_in_l',v_in_l'],v_out_l',gf,options,'problem',Vdd);
gf=fittype('tw_out_model(a0,a1,b0,b1,t0,ti,tw_in,v_in)','coefficients',{'a0','a1','b0','b1','t0','ti'},'independent',{'tw_in','v_in'});
tw_out_l=reshape(tw_out,1,size(tw_out,1)*size(tw_out,2));
options = fitoptions('Method','NonlinearLeastSquares','Robust','Off');
[tw_model,goft,rtw_model]=fit([tw_in_l',v_in_l'],tw_out_l',gf,options);
minV = min(rv_model.residuals);
maxV = max(rv_model.residuals);
avV = mean(rv_model.residuals);
minT = min(rtw_model.residuals);
maxT = max(rtw_model.residuals);
avT = mean(rtw_model.residuals);
end

