function [tw_in, v_in] = gen_sweep_pars(Vdd,max_tw,n_v,n_t)
v_ratio =0.7; %how much of the points concentrated in the middle
t_ratio =0.7; 

v_lim1=0.5*Vdd;
v_lim2=0.8*Vdd;
v_mid = linspace(v_lim1,v_lim2,ceil(n_v*v_ratio));
v_low =  linspace(0.2,v_lim1,ceil(0.5*n_v*(1-v_ratio)));
v_high= linspace(v_lim2,Vdd,floor(0.5*n_v*(1-v_ratio)));

v_in = unique([v_low v_mid v_high]);


t_lim1=0.5*max_tw;
t_lim2=0.8*max_tw;
t_mid = linspace(t_lim1,t_lim2,ceil(n_t*t_ratio));
t_low =  linspace(10*10^-12,t_lim1,ceil(0.5*n_t*(1-t_ratio)));
t_high= linspace(t_lim2,max_tw,floor(0.5*n_t*(1-t_ratio)));

tw_in = unique([t_low t_mid t_high]);

%out_combs=combvec(v_in,tw_in);
%v_in = out_combs(1,:);
%tw_in = out_combs(2,:);
 %t_c = [num2str(tw_in)]
%v_c = [num2str(v_in)]
end