clear
Vdd=3;
plot_trans=0;
filename='INVX0_DSVP_2500_nozeros.csv';
%filename='test1_INVX0_targetted_2500pt.csv';
%filename='test3.csv';
correct_output=Vdd;
v_threshold=0.04;

fid=fopen(filename);
tags = fgetl(fid);
fclose(fid);
%close all
data = csvread(filename,1,0);
data_X=data(:,1:2:end);
data_Y=data(:,2:2:end);


[tw_in_strs]= regexp(tags,'tw_in=[\w\d\.\+\-]*','match');
[v_in_strs]= regexp(tags,'v_in=[\w\d\.\+\-]*','match');

tw_in_val = strsplit(strjoin(tw_in_strs),{' ','='});
tw_in_val=tw_in_val(2:2:end);%to remove tw_in
tw_in_val=tw_in_val(1:2:end);% to remove duplicated vals (\X \Y)

v_in_val =strsplit(strjoin(v_in_strs),{' ','='});
v_in_val=v_in_val(2:2:end);
v_in_val=v_in_val(1:2:end);% to remove duplicated vals (\X \Y)


% determine v_out and tw_out

for indx =1:size(data_Y,2)%[390 391 392]
    %filter data to remove zeros added by spectre
    indx_non_zero_time = data_X(:,indx) >0;indx_non_zero_time(1)=1;
    data_X_filtered=data_X(indx_non_zero_time,indx);
    data_Y_filtered=data_Y(indx_non_zero_time,indx);
    if(correct_output > 0)
        data_Y_temp=correct_output-(data_Y_filtered);
    else
        data_Y_temp=data_Y_filtered;
    end
    if(plot_trans)
     figure
     %hold on
        plot(data_X_filtered,data_Y_filtered,'.');
    ttl=['tw\_in= ',tw_in_val{indx},'ps v\_in= ' ,v_in_val{indx}];
    title(ttl)
    end
    [v_out(indx), indx_max]=max(data_Y_temp);
    tw_in(indx)=str2double(tw_in_val{indx});
    v_in(indx)=str2double(v_in_val{indx});
    if(abs(v_out(indx))>v_threshold)
        difference = abs(v_out(indx)/2-data_Y_temp);
        [~,first_edge] = min(difference(1:indx_max-1));

        [~,secnd_edge] = min(difference(indx_max+1:end));
        if(indx_max < length(difference))
            secnd_edge=secnd_edge+indx_max;
        else
            secnd_edge = indx_max;
        end
        if(isempty(first_edge))
                first_edge=secnd_edge;
        end
        tw_out(indx)=abs(data_X_filtered(first_edge)-data_X_filtered(secnd_edge));
     if(plot_trans)
    hold on
    plot(data_X_filtered(first_edge),data_Y_filtered(first_edge),'ro');
    plot(data_X_filtered(secnd_edge),data_Y_filtered(secnd_edge),'ro');
     end
    else
        tw_out(indx)= 0;
    end

end
%%
figure(1)
tw_in=tw_in*10^12;
tw_out=tw_out*10^12;
%to plot surface
nn=length(unique(v_in));
v_in =reshape(v_in,nn,length(v_in)/nn);
tw_in =reshape(tw_in,nn,length(tw_in)/nn);
v_out =reshape(v_out,nn,length(v_out)/nn);
tw_out =reshape(tw_out,nn,length(tw_out)/nn);
%scatter3(tw_in,v_in,v_out);
surf(tw_in,v_in,v_out);
xlabel('tw\_in');ylabel('v\_in');zlabel('v\_out');
figure(2)
%scatter3(tw_in,v_in,tw_out);
surf(tw_in,v_in,tw_out);
xlabel('tw\_in');ylabel('v\_in');zlabel('tw\_out');
ylim([Vdd/2 Vdd])
%
% v_in=v_in(3:end,:);
% tw_in=tw_in(3:end,:);
% v_out=v_out(3:end,:);