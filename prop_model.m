function [dV] = prop_model(alpha,c,T,VDC,td1,Vdd,tw_in,v_in)
%VDC=1.55;
%td1=70;

dV = Vdd./(1+exp(-(c*(1-exp(-tw_in./T))).*(v_in-(VDC*(1+(td1./tw_in).^alpha)))));

%g=fittype(@(alpha,c,T,VDC,td1,x,y) 1./(1+exp(-(c*(1-exp(-x./T))).*(y-(VDC*(1+(td1./x).^alpha))))));

end
% 
%      General model:
%      ans(tw_in,v_in) = prop_model(alpha,c,T,tw_in,v_in)
%      Coefficients (with 95% confidence bounds):
%        c =      0.2271  (-3.769e+08, 3.769e+08)
%        T =       1.716
%        alpha =      -9.823
