cells = {'NA2_3VX4'};%{'IN_3VX4'};%{'AND2_3VX0'};%{'NO2_3VX0'};%{'IN_3VX0'};%, 'AND4_3VX1'};
ports_conns = { {'A' 'B' 'Q'; 2 1 3 } ,{'A' 'B' 'C' 'D' 'Q'; 1 0 1 2 3 } };%%%%%%%
netlists_path = '~/simulation';
%
for cell_id = 1:length(cells)
    cells{cell_id}
    my_run_dir = [netlists_path,'/',cells{cell_id},'/my_run'];
    gate_netlist_path = [netlists_path,'/',cells{cell_id},'/spectre/schematic/netlist/netlist'];
    mkdir(my_run_dir);
    
[sim_netlist,instnce_name]=create_sim_netlist(gate_netlist_path,ports_conns{cell_id});
[ocn_script]= create_sim_ocean(sim_netlist,my_run_dir);
cmd = ['./run_ocean.sh ',ocn_script , ' > ',[my_run_dir,'/my_run.log']];
system(cmd);
end
%%
report_path = './';
report_file = [report_path,'output_report.csv'];
heading = char({'Cell Name,Min_V_Error,Max_V_Error,Avg_V_Error,Min_T_Error,Max_T_Error,Avg_T_Error'});
dlmwrite(report_file,heading,'delimiter','');
correct_output_vector = [0];
 for cell_id = 1:length(cells)
[tw_in,v_in,tw_out,v_out]=parse_sim('sim.out',correct_output_vector(cell_id));
[minV,maxV,avV,minT,maxT,avT] = calc_error(tw_in,v_in,tw_out,v_out);
dlmwrite(report_file,{[cells{cell_id},',',num2str(minV),',',num2str(maxV),',',num2str(avV),',',num2str(minT),',',num2str(maxT),',',num2str(avT)]},'delimiter','','-append');
 end
