function [tw_out] = tw_out_model(a0,a1,b0,b1,t0,ti,tw_in,v_in)
tw_out = (a0 +a1*v_in).*tw_in  + (b0 + b1*v_in).*tw_in.^2;
%tw_out = (a0 +a1*v_in).*tw_in + t0.*exp(-tw_in/ti) + (b0 + b1*v_in).*tw_in.^2;

end

